
var bird
var grass
var pig
var startX = 1000 / 4
var startY = 400 * 1 / 2 + 20
const gravity = 0.09
const airDrag = 0.001
let released = false
var back

function setup() {
    var birdImg = loadImage("redbird.png")
    var grassImg = loadImage("grass.png")
    var pigImg = loadImage("pigImg.png")
    back = loadImage('AngryBirdsBackground.jpg')
    createCanvas(2000, 400)
    ground = new Group()
    pigs = new Group()
    bird = createSprite(width/8, height * 1 / 2 + 20, 20, 20)

    for(var i = 0; i*600 < width; i++){
        grass = createSprite(i*600, height * 3 / 4 + 70, 800, 0)
        grass.addImage(grassImg)
        grass.setCollider('rectangle', 0, 0, 800, 80)
        grass.friction = 0.8
        grass.immovable = true
        ground.add(grass)
    }

    for (var i = 0; i < 9; i++) {
        pig = createSprite(width * 1 / 4 + i * 40, height * 1 / 2 + 75, 30, 30)
        pig.addImage(pigImg)
        pig.scale = 0.35
        //pig.setCollider('circle', 0, 0, 30)
        pig.immovable = false
        pig.velocity.y = 1
        pigs.add(pig)
    }

    bird.mouseActive = true
    bird.scale = 0.02
    bird.addImage(birdImg)
    bird.onMouseMoved = function() {
    }
}

function draw() {
    image(back, 0, 0, width, height)
    stroke(0)
    strokeWeight(7)
    line(startX, startY, startX, startY + 100)
    stroke('brown')
    strokeWeight(3)
    bird.rotationSpeed = - Math.abs(bird.velocity.y)
    bird.collide(ground, bounce)
    bird.bounce(pigs)
    pigs.bounce(pigs)
    pigs.bounce(ground)


    if (mouseIsPressed && !released) {
        line(startX, startY, bird.position.x, bird.position.y)
        bird.velocity.x = (mouseX - bird.position.x) * 0.2
        bird.velocity.y = (mouseY - bird.position.y) * 0.2
    }

    if(released){
        if ((releasePointX < startX && bird.position.x > startX) || (releasePointX > startX && bird.position.x < startX)) {
            bird.velocity.y += gravity
            bird.velocity.x -= airDrag
        } else {
            line(startX, startY, bird.position.x, bird.position.y)
        }
    }

    if(bird.position.y > height || bird.position.x > width || bird.position.x < 0) {
        resetToStart()
    }

    drawSprites()
}

function bounce() {
    if(released) {
        bird.velocity.y *= - 0.65
        bird.velocity.x *= 0.65
    }
}

function mouseReleased() {
    var force = (Math.abs(mouseX -startX) + Math.abs(mouseY-startY)) / 15
    var launchAngle = Math.atan2((mouseY - startY), (mouseX - startX)) * 180 / Math.PI + 180
    if(force > 3 && !released) {
        bird.setSpeed(force, launchAngle)
        released = true
        releasePointX = mouseX
    } else {
        resetToStart()
    }
}

function resetToStart() {
    bird.position.x = startX
    bird.position.y = startY
    bird.velocity.y = 0
    bird.velocity.x = 0
    bird.rotation = 0
    released = false
}

function mousePressed() {

}

